# 1. Create an abstract class called Animal that has the following abstract methods    
# Abstract Methods: 
# eat(food), make_sound()    
# 2. Create two classes that implements the Animal class called Cat and Dog with each of the following properties and methods:    
# Properties:      
# name, breed, age    
# Methods:      
# getters and setters, implementation of abstract methods, call()

from abc import ABC, abstractclassmethod

class Animal(ABC):
    @abstractclassmethod
    def eat(self, food):
        pass
    @abstractclassmethod
    def make_sound(self):
        pass

class Cat(Animal):
    def __init__(self, name, breed, age):
        super().__init__()

        self._name = name
        self._breed = breed
        self._age = age

    # getters    
    def get_name(self):
        return self._name
    def get_breed(self):
        return self._breed
    def get_age(self):
        return self._age

    # setters
    def set_name(self, name):
        self._name = name
    def set_name(self, breed):
        self._breed = breed 
    def set_name(self, age):
        self._age = age

    def eat(self, food):
        print(f"Serve me {food}")

    def make_sound(self):
        print(f"Miaow! Nyaw! Nyaaaa!")

    def call(self):
        print(f"{self._name}, come on!")                 

class Dog(Animal):
    def __init__(self, name, breed, age):
        super().__init__()

        self._name = name
        self._breed = breed
        self._age = age

    # getters    
    def get_name(self):
        return self._name
    def get_breed(self):
        return self._breed
    def get_age(self):
        return self._age

    # setters
    def set_name(self, name):
        self._name = name
    def set_name(self, breed):
        self._breed = breed 
    def set_name(self, age):
        self._age = age

    def eat(self, food):
        print(f"Eaten {food}")

    def make_sound(self):
        print(f"Bark! Woof! Arf!")

    def call(self):
        print(f"Here {self._name}!")

cat = Cat("Puss", "Siamese", 2)
dog = Dog("Isis", "Choco Lab", 3)

dog.eat("Steak")
dog.make_sound()
dog.call() 

cat.eat("Tuna")
cat.make_sound()
cat.call()             